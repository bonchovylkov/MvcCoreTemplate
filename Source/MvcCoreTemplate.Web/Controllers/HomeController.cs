﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MvcCoreTemplate.Services.Data;

namespace MvcCoreTemplate.Web.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(ICommonService commonService) : base(commonService)
        {
        }

        public IActionResult Index()
        {
            var model = this.commonService.TestService();
            return View("Index",model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
