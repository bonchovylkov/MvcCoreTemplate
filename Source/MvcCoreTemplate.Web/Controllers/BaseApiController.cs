using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MvcCoreTemplate.Services.Data;

namespace MvcCoreTemplate.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/BaseApi")]
    public class BaseApiController : Controller
    {
        private ICommonService commonService;
        public BaseApiController(ICommonService commonService)
        {
            this.commonService = commonService;
        }
        public string TestService()
        {
            return this.commonService.TestService();
        }
    }
}