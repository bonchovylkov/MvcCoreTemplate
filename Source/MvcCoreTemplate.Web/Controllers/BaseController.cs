using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MvcCoreTemplate.Services.Data;

namespace MvcCoreTemplate.Web.Controllers
{
    public class BaseController : Controller
    {
        protected ICommonService commonService;
        public BaseController(ICommonService commonService)
        {
            this.commonService = commonService;
        }

      
    }
}