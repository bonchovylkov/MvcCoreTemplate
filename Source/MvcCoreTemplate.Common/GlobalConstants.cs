﻿using System;

namespace MvcCoreTemplate.Common
{
    public class GlobalConstants
    {
        public const string AdministratorRoleName = "Administrator";
        public const string AppName = "MvcCoreTemplate";
        
    }
}
