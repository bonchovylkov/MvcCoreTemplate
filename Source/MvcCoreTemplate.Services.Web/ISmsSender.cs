﻿using System.Threading.Tasks;

namespace MvcCoreTemplate.Services.Web
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}
