﻿using System.Threading.Tasks;

namespace MvcCoreTemplate.Services.Web
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
